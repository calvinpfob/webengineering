 $(document).ready(function(){
   $(".dropdown-trigger").dropdown();
 });

 $(document).ready(function(){
     $('.collapsible').collapsible();
});

$("#login-button").click(function(){
    fetch('/login', {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
        username: $("#username").val(),
        password: $("#password").val()
      })
    }).then(res => res.json()).then(function(response){
      if(response.logged_in){
        $("#user").html($("#username").val());
        load_page("home");
      }else{
        $("#username").addClass("invalid");
        $("#password").addClass("invalid");
      }
    });
  });

  $(function() {
    // pressing enter
      $("#searchTerm").keypress(function(e){
      	if(e.keyCode===13){
      		var searchTerm = $("#searchTerm").val();
  		    var url = "https://de.wikipedia.org/w/api.php?action=opensearch&search="+ searchTerm +"&format=json&callback=?";
  		    $.ajax({
  			url: url,
  			type: 'GET',
  			contentType: "application/json; charset=utf-8",
  			async: false,
          	dataType: "json",
          	success: function(data, status, jqXHR) {
          		//console.log(data);
          		$("#output").html(""); //delete old stuff
          		for(var i=0;i<data[1].length;i++){
                //data[1] hat die überschriften. data[3] sind die links zur wikipedia-Webseite.
                //data[2] gibt den ersten Satz des Eintrags zurück. über length wird bestimmt,
                //wie viele Sucheinträge gefunden worden sind.
          			$("#output").prepend("<div class='card-panel teal darken-1'>"+"<div><div class='well'><a href="+data[3][i]+"><h5>"+"<span class='teal darken-1 teal white-text'>" + data[1][i]+ "</span></h5>" + "<p>" +"<span class='teal darken-1 teal white-text'>"+ data[2][i] + "</p></span></a></div></div></div>");
          		}
              $("#improve").show();

          	}
  		})
      	}
      });
  // click ajax call
  //when you press auf suche
      $("#search").on("click", function() {
      	var searchTerm = $("#searchTerm").val();
  		var url = "https://de.wikipedia.org/w/api.php?action=opensearch&search="+ searchTerm +"&format=json&callback=?";
  		$.ajax({
  			url: url,
  			type: 'GET',
  			contentType: "application/json; charset=utf-8",
  			async: false,
          	dataType: "json",
            // plop data
          	success: function(data, status, jqXHR) {
          		//console.log(data);
          		$("#output").html(""); //delete old stuff
          		for(var i=0;i<data[1].length;i++){
          			$("#output").prepend("<div class='card-panel teal darken-1'>"+"<div><div class='well'><a href="+data[3][i]+"><h5>"+"<span class='teal darken-1 teal white-text'>" + data[1][i]+ "</span></h5>" + "<p>" +"<span class='teal darken-1 teal white-text'>"+ data[2][i] + "</p></span></a></div></div></div>");
          		}

          	}
  		})
  		.done(function() {
  			console.log("success");
  		})
  		.fail(function() {
  			console.log("error");
  		})
  		.always(function() {
  			console.log("complete");
  		});


      });
  });

  $(document).ready(function(){
      $('.sidenav').sidenav();
    });


    $(document).ready(function(){
      $('.modal').modal();
    });
