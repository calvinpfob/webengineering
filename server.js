const express = require('express');
//const app = express();
const bodyParser = require('body-parser');
const port = 3000;

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(bodyParser.json());
app.use(express.static('public'));


app.get('/', function(req, res){
  res.send('Hello World!');
});
/*
app.post('/login', function (req, res) {
  let logged_in = is_user_logged_in(req.body.username,req.body.password);
  if(logged_in){
    req.session.user = req.body.username;
    req.session.password = req.body.password;
    logged_in = true;
  }
  res.send('{"logged_in":"'+logged_in+'"}');
})

function is_user_logged_in(session_user,session_password){
  let logged_in = false;
  if(session_user){
    users.forEach(function(user){
      if(user.username == session_user && user.password == session_password){
        logged_in = true;
      }
    });
  }
  return logged_in;
}

$("#login-button").click(function(){
    fetch('/login', {
      method: 'post',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify({
        username: $("#username").val(),
        password: $("#password").val()
      })
    }).then(res => res.json()).then(function(response){
      if(response.logged_in){
        $("#user").html($("#username").val());
        load_page("home");
      }else{
        $("#username").addClass("invalid");
        $("#password").addClass("invalid");
      }
    });
  });
*/
var db = [];

db.push({text:"hei"});

db.push({text:"Hallo"});


app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

//io.on('connection', function(socket){
//  console.log('a user connected');
//});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    console.log('message: ' + msg);
  });
});

io.emit('some event', { for: 'everyone' });

io.on('connection', function(socket){
  socket.on('chat message', function(msg){

    heute = new Date();
    let hour = heute.getHours();
    let min = heute.getMinutes();
    let sec = heute.getSeconds();
    sendN(msg);

    io.emit('chat message', hour+":"+min+":"+sec+" "+msg);
  });
});





























//by Matthias Hehn

const webPush = require('web-push')

process.env.VAPID_EMAIL = "email@mywebstage.de";
process.env.VAPID_PUBLIC_KEY = 'BPV2JQcbcoPN6neKKsRpbvQBqVb3IMXh4VW3kG_udE0phqz1insvQsgywCRPG_RC_AOzs7GzPORclEBFqfZRIPE';
process.env.VAPID_PRIVATE_KEY = '0BLRAHwC2vgoVD5BGqHqGizh5u0OYtAu6sgakczhm0U';

webPush.setVapidDetails(
  'mailto:'+process.env.VAPID_EMAIL,
  process.env.VAPID_PUBLIC_KEY,
  process.env.VAPID_PRIVATE_KEY
);
var subscriptions = [];
function sendN(payload) {
  Object.values(subscriptions).forEach(function(subscription){
    console.log(subscription);
    webPush.sendNotification(subscription,payload).then(function() {
      console.log("send");
    }).catch(function(err) {
      console.log(err);
      delete subscriptions[subscription.endpoint];
    });
  });
}

app.post('/push/register', function(req, res) {
  var subscription = req.body.subscription;
  if (!subscriptions[subscription.endpoint]) {
    subscriptions[subscription.endpoint] = subscription;
  }
  res.sendStatus(201);
});

app.get('/push/vapidPublicKey', function(req, res) {
  res.send(process.env.VAPID_PUBLIC_KEY);
});
